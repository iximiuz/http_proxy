#include "clients_storage.h"
#include "dispatch.h"
#include "http_client_events.h"
#include "proxy_client.h"
#include "proxy_daemon.h"
#include "proxy_worker.h"
#include "worker_events.h"

#include <assert.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define MAXEVENTS 64


extern struct proxy_daemon g_daemon;

static int epoll_add(int efd, int sfd)
{
    int s;
    struct epoll_event event;

    event.data.fd = sfd;
    event.events = EPOLLIN | EPOLLOUT | EPOLLET; // EPOLLRDHUP ??
    s = epoll_ctl(efd, EPOLL_CTL_ADD, sfd, &event);
    if (s == -1) {
        return CAN_NOT_ADD_FD_TO_EPOLL;
    }

    return 0;
}

static void accept_incoming_connections(int efd, int sfd, CLIENT_TYPES type)
{
    int s;

    while (1) {
        struct sockaddr in_addr;
        socklen_t in_len;
        int infd;

        in_len = sizeof in_addr;
        infd = accept(sfd, &in_addr, &in_len);
        if (infd == -1) {
            if (errno != EAGAIN && errno != EWOULDBLOCK) {
                fprintf(stderr, "Accept error: errno [%d]\n", errno);
            }

            break;
        }

        // Make the incoming socket non-blocking and add it to the list of fds to monitor.
        s = make_socket_non_blocking(infd);
        if (s == -1) {
            fprintf(stderr, "Can not make socket [fd %d] non blocking: errno [%d]\n", infd, errno);
            close(infd);
            break;
        }

        if (0 != epoll_add(efd, infd)) {
            fprintf(stderr, "Can not add socket [fd %d] to epoll\n", infd);
            close(infd);
            break;
        }

        if (NULL == clients_storage_add_client(infd, type)) {
            fprintf(stderr, "Can not save incoming connection [fd %d]. Allocation failed\n", infd);
            close(infd);
            break;
        }

#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
        printf("New incoming connection [fd %d] with type [%d]\n", infd, type + 1);
        if (CLIENT_TYPE_HTTP_CLIENT == type) {
            printf("[+] Connected new HTTP client [fd %d]\n", infd);
        }
        fflush(stdout);
#endif

        g_daemon.incoming_connections_count[type]++;
    }
}

/**
 * Create non-blocking socket and start listening on it.
 *
 * @param host
 * @param port
 * @param sfd [out] created socket file descriptor
 * @return operation status
 */
static int init_socket(const char *host, const char *port, int *sfd)
{
    int s;

    *sfd = create_and_bind_non_blocking_socket(port, host);
    if (*sfd == -1) {
        return CAN_NOT_CREATE_OR_BIND_SOCKET;
    }

    s = listen(*sfd, SOMAXCONN);
    if (s == -1) {
        return CAN_NOT_START_LISTEN_SOCKET;
    }

    return 0;
}

static void disconnect_client(const int target_sfd)
{              
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
    printf("Disconnect socket [fd %d]\n", target_sfd);
    fflush(stdout);
#endif

    proxy_client *client = clients_storage_get_client_by_sfd(target_sfd);
    if (NULL != client) {
        g_daemon.incoming_connections_count[client->type]--;
        clients_storage_remove_client(target_sfd);
    }

    close(target_sfd);
}

static proxy_client *safe_fetch_client(int sfd)
{
    proxy_client *client = clients_storage_get_client_by_sfd(sfd);
    if (NULL == client) {
        fprintf(stderr, "dispatch: Cannot find client for descriptor [%d]\n", sfd);
        close(sfd);
    }

    return client;
}

static void read_from_socket_any_type(int sfd)
{
    proxy_client *client = safe_fetch_client(sfd);
    if (NULL == client) {
        return;
    }

    if (CLIENT_TYPE_WORKER == client->type) {
        if (WORKER_EVENT_ABORT == do_worker_state_transition((worker *)client, WORKER_EVENT_DATA_IN)) {
            disconnect_client(sfd);
        }
    } else {
        if (HTTP_CLIENT_EVENT_ABORT == do_http_client_state_transition((http_client *)client, HTTP_CLIENT_EVENT_DATA_IN)) {
            disconnect_client(sfd);
        }
    }
}

static void write_to_socket_any_type(int sfd)
{
    proxy_client *client = safe_fetch_client(sfd);
    if (NULL == client) {
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_ERROR
        printf("Client for socket [fd %d] not found to write data.\n", sfd);
        fflush(stdout);
#endif
        return;
    }

    if (CLIENT_TYPE_WORKER == client->type) {
        if (WORKER_EVENT_ABORT == do_worker_state_transition((worker *)client, WORKER_EVENT_DATA_OUT)) {
            disconnect_client(sfd);
        }
    } else {
        if (HTTP_CLIENT_EVENT_ABORT == do_http_client_state_transition((http_client *)client, HTTP_CLIENT_EVENT_DATA_OUT)) {
            disconnect_client(sfd);
        }
    }
}

int dispatch(const char *host, const char *http_client_port, const char *worker_port)
{
    int http_client_sfd, worker_sfd, s, efd;
    struct epoll_event *events;

    // create socket and start listen on port1
    s = init_socket(host, http_client_port, &http_client_sfd);
    if (0 != s) {
        return s;
    }
    g_daemon.fds[CLIENT_TYPE_HTTP_CLIENT] = http_client_sfd;
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
    printf("Daemon HTTP client's port [%s] is bind to fd [%d]\n", http_client_port, http_client_sfd);
    fflush(stdout);
#endif

    // create socket and start listen on port2
    s = init_socket(host, worker_port, &worker_sfd);
    if (0 != s) {
        return s;
    }
    g_daemon.fds[CLIENT_TYPE_WORKER] = worker_sfd;
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
    printf("Daemon worker's port [%s] is bind to fd [%d]\n", worker_port, worker_sfd);
    fflush(stdout);
#endif


    efd = epoll_create1(0);
    if (efd == -1) {
        return CAN_NOT_CREATE_EPOLL;
    }

    s = epoll_add(efd, http_client_sfd);
    if (0 != s) {
        return s;
    }

    s = epoll_add(efd, worker_sfd);
    if (0 != s) {
        return s;
    }


    // Buffer where events are returned
    events = calloc(MAXEVENTS, sizeof(struct epoll_event));

    // The event loop
    while (1) {
        int n, i;

        n = epoll_wait(efd, events, MAXEVENTS, -1);
        if (-1 == n) {
            if (EINTR == errno) {
                // Certain signal handler will interrupt epoll_wait()
                continue;
            }

            fprintf(stderr, "epoll_wait failure: errno [%d]\n", errno);
            abort();
        }

        for (i = 0; i < n; i++) {
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
            printf("[+] New epoll event [%d] for socket [fd %d]\n", events[i].events, events[i].data.fd);
            fflush(stdout);
#endif

            if ((events[i].events & EPOLLERR) || (events[i].events & EPOLLHUP)) {
                // An error has occured on this fd, or the socket is not ready for reading/writing
                if (http_client_sfd == events[i].data.fd || worker_sfd == events[i].data.fd) {
                    fprintf(stderr,
                            "Unexpected epoll error on HTTP client's or worker's socket [fd %d]."
                            "Daemon is shutting down\n",
                            events[i].data.fd);
                    abort();
                } else {
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
                    printf("Unexpected epoll error on client or worker socket [fd %d]\n", events[i].data.fd);
                    fflush(stdout);
#endif
                    disconnect_client(events[i].data.fd);
                }
                continue;
            }

            if ((http_client_sfd == events[i].data.fd || worker_sfd == events[i].data.fd)) {
                assert(events[i].events & EPOLLIN);

                // incoming connection(s) from client1 or client2
                accept_incoming_connections(efd,
                                            events[i].data.fd,
                                            http_client_sfd == events[i].data.fd
                                                ? CLIENT_TYPE_HTTP_CLIENT
                                                : CLIENT_TYPE_WORKER);
                continue;
            }

            if (events[i].events & EPOLLIN) {
                read_from_socket_any_type(events[i].data.fd);
                continue;
            }

            if (events[i].events & EPOLLOUT) {
                write_to_socket_any_type(events[i].data.fd);
                continue;
            }

            fprintf(stderr, "Unexpected epoll event [%d] for socket [%d]. Skipping it.\n",
                    events[i].events, events[i].data.fd);
        }
    }

    free(events);
    close(http_client_sfd);
    close(worker_sfd);

    return 0;
}
