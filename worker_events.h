#ifndef WORKER_EVENTS_H
#define WORKER_EVENTS_H

#include "proxy_client.h"

typedef enum {
    WORKER_EVENT_NO_EVENT,
    WORKER_EVENT_TRANSIT,  // unconditional transition
    WORKER_EVENT_DATA_IN,  // Направление данных "от воркера к демону"
    WORKER_EVENT_DATA_OUT, // Направление данных "от демона к воркеру"
    WORKER_EVENT_NEW_HTTP_CLIENT,
    WORKER_EVENT_ABORT
} WORKER_EVENTS;


WORKER_EVENTS do_worker_state_transition(worker *w, WORKER_EVENTS event);

#endif // WORKER_EVENTS_H
