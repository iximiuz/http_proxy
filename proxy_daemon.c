#include "proxy_daemon.h"


struct proxy_daemon g_daemon;

void reset_daemon(struct proxy_daemon *daemon)
{
    daemon->ports[CLIENT_TYPE_HTTP_CLIENT] = UNDEFINED_PORT;
    daemon->ports[CLIENT_TYPE_WORKER] = UNDEFINED_PORT;
    daemon->fds[CLIENT_TYPE_HTTP_CLIENT] = UNDEFINED_SFD;
    daemon->fds[CLIENT_TYPE_WORKER] = UNDEFINED_SFD;
    daemon->incoming_connections_count[CLIENT_TYPE_HTTP_CLIENT] = 0;
    daemon->incoming_connections_count[CLIENT_TYPE_WORKER] = 0;
}
