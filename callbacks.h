#ifndef CALLBACKS_H
#define CALLBACKS_H

#include "proxy_client.h"


int on_read_http_request(int sfd, const char *data, int size);
int on_read_worker_response(int sfd, const char *data, int size);
void on_complete_write_to_client_any_type(proxy_client *client);

#endif // CALLBACKS_H
