#ifndef PROXY_CLIENT_H
#define PROXY_CLIENT_H

#include "buffer.h"

#include "http-parser/http_parser.h"

#define CLIENT_TYPES_COUNT 2


typedef enum {
    CLIENT_TYPE_HTTP_CLIENT,
    CLIENT_TYPE_WORKER
} CLIENT_TYPES;

typedef enum {
    HTTP_CLIENT_STATE_CONNECTED = 100,
    HTTP_CLIENT_STATE_WAIT_FOR_FREE_WORKER,
    HTTP_CLIENT_STATE_READING_REQUEST,
    HTTP_CLIENT_STATE_REQUEST_IS_READ,
    HTTP_CLIENT_STATE_REQUEST_PROCESSING,
    HTTP_CLIENT_STATE_BAD_REQUEST,

    WORKER_STATE_INITIAL = 200,
    WORKER_STATE_HANDSHAKE,
    WORKER_STATE_READY,
    WORKER_STATE_PACKET_PROCESSING,
    WORKER_STATE_WRITING_HTTP_REQUEST,
    WORKER_STATE_HTTP_REQUEST_PROCESSING,
    WORKER_STATE_READING_HTTP_RESPONSE,
    WORKER_STATE_FORWARD_HTTP_RESPONSE,
    WORKER_STATE_BAD_PROTO,

    CLIENT_STATE_UNDEFINED = 300,
    CLIENT_STATE_CONSUMING,  // Направление данных "От клиента к демону"
    CLIENT_STATE_SUPPLYING,   // Направление данных "От демона к клиенту"
    CLIENT_STATE_DISCONNECT
} CLIENT_STATES;


typedef struct _proxy_client {
    int sfd;
    CLIENT_TYPES type;
    CLIENT_STATES state;
    CLIENT_STATES next_state;
    CLIENT_STATES prev_state;
} proxy_client;

typedef struct _http_client {
    proxy_client base;
    int worker_sfd;
    http_parser *http_parser;
} http_client;

typedef struct _chunked_buffer {
    buffer *bucket;
    struct _chunked_buffer *next;
} chunked_buffer;

typedef struct _worker {
    proxy_client base;
    int http_client_sfd;
    chunked_buffer *buf;
} worker;

#endif // PROXY_CLIENT_H
