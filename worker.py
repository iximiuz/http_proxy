import socket
import struct
import logging
import re


class Client:
    HANDSHAKE_REQUEST = 'ping'
    HANDSHAKE_RESPONSE = 'pong'

    FIELD_SIZE_MESSAGE_TYPE = 1
    FIELD_SIZE_MESSAGE_SIZE = 4
    FIELD_SIZE_MESSAGE_FLAGS = 1

    MESSAGE_TYPE_HTTP_REQUEST = 0x01
    MESSAGE_TYPE_HTTP_RESPONSE = 0x02
    MESSAGE_TYPE_HTTP_RESPONSE_ACK = 0x03

    FLAG_MASK_FIN = 0x03
    FLAG_MASK_RST = 0x02

    def __init__(self, ip, port, logger):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._ip = ip
        self._port = port
        self._state = 'initial'
        self._logger = logger

    def connect(self):
        self._socket.connect((self._ip, self._port))

        self._write(Client.HANDSHAKE_REQUEST)
        response = self._read(len(Client.HANDSHAKE_RESPONSE))

        if 'pong' == response:
            self._state = 'connected'
        else:
            raise Exception('connection failed [reason: ' + response + ']')

    def start(self):
        self.connect()

        self._state = 'proxying_request'

        request = ''
        inet_socket = None
        while True:
            request_part, has_more_data = self._read_http_request()
            self._logger.debug(request_part)

            if None == inet_socket:
                request += request_part
                m = re.search('^Host:\s+(www\.)?(.*)$', request, re.MULTILINE)
                if m:
                    inet_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    inet_socket.connect((m.group(2).strip(), 80))

            if None != inet_socket:
                inet_socket.sendall(request_part)

            if not has_more_data:
                break

        self._state = 'proxying_response'

        self._write_http_response(inet_socket)

    def _disconnect(self):
        self._socket.shutdown()
        self._socket.close()

    def _read(self, size):
        # todo: read by parts with random length and sleep between
        res = ''
        while len(res) < size:
            part = self._socket.recv(size - len(res))
            self._logger.debug(repr(part))
            res += part

        return res

    def _read_http_request(self):
        self._read_message_type(Client.FIELD_SIZE_MESSAGE_TYPE)
        size = self._read_message_size()
        flags = self._read_message_flags()

        return self._read(size), not self._test_flag(flags, Client.FLAG_MASK_FIN)

    def _read_message_flags(self):
        return struct.unpack('B', self._read(Client.FIELD_SIZE_MESSAGE_FLAGS))[0]

    def _read_message_size(self):
        size = struct.unpack('i', self._read(Client.FIELD_SIZE_MESSAGE_SIZE))[0]
        if size < 0:
            raise Exception('Unexpected message size [' + repr(size) + ']')

        return size

    def _read_message_type(self, expected=None):
        message_type = self._read(Client.FIELD_SIZE_MESSAGE_TYPE)
        if None != expected and expected != struct.unpack('B', message_type)[0]:
            raise Exception('Unexpected message type [' + repr(message_type) + ']. Expected [' + repr(expected) + ']')

        return message_type

    @staticmethod
    def _test_flag(flags, mask):
        return bool(flags & mask)

    def _write(self, data):
        # todo: send by parts with random length and sleep between
        #for c in data.split():
        #    self._socket.sendall(c)
        self._socket.sendall(data)

    def _write_http_response(self, inet_socket):
        # todo: read HTTP response and parse it to determine end of message
        while True:
            response = inet_socket.recv(16)

            response = struct.pack('B', Client.MESSAGE_TYPE_HTTP_RESPONSE)[0] + \
                       struct.pack('i', len(response))[0] + \
                       struct.pack('B', 0)[0] + response

            self._write(response)
            self._read_message_type(Client.MESSAGE_TYPE_HTTP_RESPONSE_ACK)
            size = self._read_message_size()
            if 0 != size:
                raise Exception('Ack message with body (size ' + repr(size) + ')')

            if self._test_flag(self._read_message_flags(), Client.FLAG_MASK_RST):
                break


TCP_IP = 'localhost'
TCP_PORT = 7788

logger = logging.getLogger('root')
FORMAT = "[%(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
logger.setLevel(logging.DEBUG)

client = Client(TCP_IP, TCP_PORT, logger)
client.start()

