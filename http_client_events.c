#include "callbacks.h"
#include "clients_storage.h"
#include "http_client_events.h"
#include "sockets.h"
#include "worker_events.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

static HTTP_CLIENT_EVENTS handle_transition_from_connected_state(http_client *hc, HTTP_CLIENT_EVENTS event,
                                                                 worker **w, WORKER_EVENTS *worker_event)
{
    assert(NULL != hc);
    assert(HTTP_CLIENT_STATE_CONNECTED == hc->base.state);
    assert(NULL == *w);

    worker *affected_worker;

    switch (event) {
    case HTTP_CLIENT_EVENT_DATA_IN:
        affected_worker = clients_storage_select_pending_worker();
        if (NULL != affected_worker) { // резервируем воркер, связывая его с клиентом
            affected_worker->base.state = WORKER_STATE_WRITING_HTTP_REQUEST;
            affected_worker->http_client_sfd = hc->base.sfd;

            hc->worker_sfd = affected_worker->base.sfd;
            hc->base.prev_state = hc->base.state;
            hc->base.state = CLIENT_STATE_CONSUMING;

            return HTTP_CLIENT_EVENT_TRANSIT;
        } else {
            hc->base.state = HTTP_CLIENT_STATE_WAIT_FOR_FREE_WORKER;
            return HTTP_CLIENT_EVENT_NO_EVENT;
        }
        break;

    // просто пропускаем такое событие
    case HTTP_CLIENT_EVENT_DATA_OUT:
        return HTTP_CLIENT_EVENT_NO_EVENT;
        break;

    default:
        hc->base.state = CLIENT_STATE_DISCONNECT;
        return HTTP_CLIENT_EVENT_TRANSIT;
    }
}

static HTTP_CLIENT_EVENTS handle_transition_from_consuming_state(http_client *hc, HTTP_CLIENT_EVENTS event,
                                                                 worker **w, WORKER_EVENTS *worker_event)
{
    assert(NULL != hc);
    assert(CLIENT_STATE_CONSUMING == hc->base.state);
    assert(NULL == *w);

    worker *affected_worker;

    switch (event) {
    case HTTP_CLIENT_EVENT_DATA_IN:
    case HTTP_CLIENT_EVENT_TRANSIT:
        if (SOCKET_IO_RES_SYS_ERROR == socket_read(hc->base.sfd, on_read_http_request)) {
            hc->base.state = CLIENT_STATE_DISCONNECT;
        } else {
            assert(HTTP_CLIENT_STATE_REQUEST_IS_READ == hc->base.state
                   || HTTP_CLIENT_STATE_READING_REQUEST == hc->base.state);

            affected_worker = clients_storage_select_pending_worker();
            assert(NULL != affected_worker);

            affected_worker->base.next_state = HTTP_CLIENT_STATE_REQUEST_IS_READ == hc->base.state
                    ? WORKER_STATE_HTTP_REQUEST_PROCESSING
                    : WORKER_STATE_WRITING_HTTP_REQUEST;

            *w = affected_worker;
            *worker_event = WORKER_EVENT_NEW_HTTP_CLIENT;
        }


        assert(CLIENT_STATE_DISCONNECT == hc->base.state
               || HTTP_CLIENT_STATE_READING_REQUEST == hc->base.state
               || HTTP_CLIENT_STATE_REQUEST_IS_READ == hc->base.state);
        return HTTP_CLIENT_EVENT_TRANSIT;
        break;

    default:
        hc->base.state = CLIENT_STATE_DISCONNECT;
        return HTTP_CLIENT_EVENT_TRANSIT;
    }
}

static HTTP_CLIENT_EVENTS handle_transition_from_request_processing_state(http_client *hc, HTTP_CLIENT_EVENTS event,
                                                                          worker **w, WORKER_EVENTS *worker_event)
{
    assert(NULL != hc);
    assert(HTTP_CLIENT_STATE_REQUEST_PROCESSING == hc->base.state);
    assert(NULL == *w);

    switch (event) {
    case HTTP_CLIENT_EVENT_DATA_IN:
        // пришло 0 байт по сети - отключаем этого клиента
        hc->base.state = CLIENT_STATE_DISCONNECT;
        return HTTP_CLIENT_EVENT_TRANSIT;
        break;

    default:
        hc->base.state = CLIENT_STATE_DISCONNECT;
        return HTTP_CLIENT_EVENT_TRANSIT;
    }
}

HTTP_CLIENT_EVENTS do_http_client_state_transition(http_client *hc, HTTP_CLIENT_EVENTS event)
{
    worker *w = NULL;
    WORKER_EVENTS worker_event;
    HTTP_CLIENT_EVENTS next_event = HTTP_CLIENT_EVENT_NO_EVENT;

    switch (hc->base.state) {
    case HTTP_CLIENT_STATE_CONNECTED:
        next_event = handle_transition_from_connected_state(hc, event, &w, &worker_event);
        assert(CLIENT_STATE_DISCONNECT == hc->base.state
               || HTTP_CLIENT_STATE_CONNECTED == hc->base.state // strange EPOLLOUT event on newly connected HTTP client
               || CLIENT_STATE_CONSUMING == hc->base.state
               || HTTP_CLIENT_STATE_WAIT_FOR_FREE_WORKER == hc->base.state);
        break;

    case HTTP_CLIENT_STATE_BAD_REQUEST:
        hc->base.state = CLIENT_STATE_DISCONNECT;
        next_event = HTTP_CLIENT_EVENT_TRANSIT;
        break;

    case HTTP_CLIENT_STATE_REQUEST_IS_READ:
        hc->base.state = HTTP_CLIENT_STATE_REQUEST_PROCESSING;
        next_event = HTTP_CLIENT_EVENT_NO_EVENT;
        break;

    case CLIENT_STATE_CONSUMING:
        next_event = handle_transition_from_consuming_state(hc, event, &w, &worker_event);
        break;

    case HTTP_CLIENT_STATE_REQUEST_PROCESSING:
        next_event = handle_transition_from_request_processing_state(hc, event, &w, &worker_event);
        break;

    //case CLIENT_STATE_SUPPLYING:
    //    next_event = handle_transition_from_supplying_state(w, event, hc, hc_event);
    //    break;

    case CLIENT_STATE_DISCONNECT:
        // todo: find related worker and toogle its state

        printf("CLIENT_STATE_DISCONNECT [fd %d]\n", hc->base.sfd);
        fflush(stdout);

        return HTTP_CLIENT_EVENT_ABORT;
        break;

    default:
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_ALERT
        fprintf(stderr, "Unexpected http client [fd %d] state [%d]\n", hc->base.sfd, hc->base.state);
#endif
        abort();
    }

    if (HTTP_CLIENT_EVENT_NO_EVENT != next_event) {
        next_event = do_http_client_state_transition(hc, next_event);
    }

    if (NULL != w) {
        do_worker_state_transition(w, worker_event);
    }

    return next_event;
}
