#include "sockets.h"
#include "clients_storage.h"
#include "proxy_client.h"

#include <arpa/inet.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>


#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
typedef enum {
    DEBUG_PRINT_TYPE_WRITE,
    DEBUG_PRINT_TYPE_READ
} DEBUG_PRINT_TYPES;

static void debug_print(int sfd, const char *buf, int size, DEBUG_PRINT_TYPES type)
{
    char tmp_buf[SOCKETS_READ_BUFF_SIZE + 1] = {0};
    memcpy(tmp_buf, buf, size);

    if (DEBUG_PRINT_TYPE_WRITE == type) {
        printf("Write [%d] byte(s) to socket [fd %d]: [%s]\n", size, sfd, tmp_buf);
    } else {
        printf("Read [%d] byte(s) from socket [fd %d]: [%s]\n", size, sfd, tmp_buf);
    }

    fflush(stdout);
}
#endif // HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG

int make_socket_non_blocking(int sfd)
{
    int flags, s;

    flags = fcntl(sfd, F_GETFL, 0);
    if (flags == -1) {
        perror("fcntl");
        return -1;
    }

    flags |= O_NONBLOCK;
    s = fcntl(sfd, F_SETFL, flags);
    if (s == -1) {
        perror("fcntl");
        return -1;
    }

    return 0;
}

int create_and_bind_non_blocking_socket(const char *port, const char *host)
{
    struct addrinfo hints = {0};
    struct addrinfo *result = NULL, *rp = NULL;
    int s, sfd;

    hints.ai_family = AF_UNSPEC;     // Return IPv4 and IPv6 choices
    hints.ai_socktype = SOCK_STREAM; // We want a TCP socket
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = NULL == host    // Specified interface [host] or All interfaces
            ? AI_PASSIVE
            : (AI_NUMERICHOST | AI_NUMERICSERV);

    s = getaddrinfo(host, port, &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        return -1;
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sfd == -1) {
            continue;
        }

        s = bind(sfd, rp->ai_addr, rp->ai_addrlen);
        if (s == 0) {
            // We managed to bind successfully!
            break;
        }

        close(sfd);
    }

    if (rp == NULL) {
        freeaddrinfo(result);
        fprintf(stderr, "Could not bind port [%s]\n", port);
        return -1;
    }

    freeaddrinfo(result);

    if (0 != make_socket_non_blocking(sfd)) {
        fprintf(stderr, "Could not set socket non blocking\n");
        return -1;
    }

    return sfd;
}

SOCKET_IO_RES socket_read(int sfd, on_socket_data_cb on_read_cb)
{
    assert(NULL != on_read_cb);

    while (1) {
        ssize_t count;
        char buf[SOCKETS_READ_BUFF_SIZE];

        count = read(sfd, buf, sizeof buf);
        if (count == -1) {
            // If errno == EAGAIN, that means we have read all data.
            if (errno != EAGAIN) {
                fprintf(stderr, "Error during read from socket [%d]: errno [%d]\n", sfd, errno);
                return SOCKET_IO_RES_SYS_ERROR;
            }

            break;
        }

#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
        debug_print(sfd, buf, count, DEBUG_PRINT_TYPE_READ);
#endif

        if (0 != on_read_cb(sfd, buf, count)) {
            return SOCKET_IO_RES_CB_ERROR;
        }

        if (0 == count) {
            return SOCKET_IO_RES_DISCONNECT;
        }
    }

    return SOCKET_IO_RES_SUCCESS;
}

int socket_write(int sfd, const char *data, int size, int *written)
{
    int written_count;
    *written = 0;

    do {
        written_count = write(sfd, data, size);
        if (written_count < 0) {
            if (errno != EAGAIN) {
                fprintf(stderr, "Error during write to socket [%d]: sock_errno [%d]\n", sfd, errno);
                return SOCKET_IO_RES_SYS_ERROR;
            }

            break;           
        }

#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
        debug_print(sfd, data, written_count, DEBUG_PRINT_TYPE_WRITE);
#endif

        *written += written_count;
    } while (*written < size);


    return SOCKET_IO_RES_SUCCESS;
}
