#include "clients_storage.h"
#include "proxy_client.h"
#include "proxy_daemon.h"
#include "proxy_worker.h"

#include "http-parser/http_parser.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>


static http_parser_settings empty_settings = {.on_message_begin = 0,
                                              .on_url = 0,
                                              .on_status = 0,
                                              .on_header_field = 0,
                                              .on_header_value = 0,
                                              .on_headers_complete = 0,
                                              .on_body = 0,
                                              .on_message_complete = 0};


static http_client *safe_fetch_http_client(int sfd)
{
    proxy_client *client = clients_storage_get_client_by_sfd(sfd);
    if (NULL == client) {
        fprintf(stderr, "Cannot find client for descriptor [fd %d]. Stop reading.\n", sfd);
    } else {
        assert(CLIENT_TYPE_HTTP_CLIENT == client->type);
    }

    return (http_client *)client;
}

static int on_request_message_complete(http_parser *parser)
{
    assert(NULL != parser->data);

    http_client *an_http_client = parser->data;
    assert(HTTP_CLIENT_STATE_READING_REQUEST == an_http_client->base.state);

    an_http_client->base.state = HTTP_CLIENT_STATE_REQUEST_IS_READ;

#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
    printf("HTTP request from client [fd %d] completely received by daemon\n", an_http_client->base.sfd);
    fflush(stdout);
#endif

    return 0;
}


int on_read_http_request(int sfd, const char *data, int size)
{
    http_client *client = safe_fetch_http_client(sfd);
    assert(NULL != client);
    assert(UNDEFINED_SFD != client->worker_sfd);

    if (0 == size) {
        client->base.state = CLIENT_STATE_DISCONNECT;
        return -1;
    }

    if (NULL == client->http_parser) {
        assert(HTTP_CLIENT_STATE_CONNECTED == client->base.prev_state);
        assert(CLIENT_STATE_CONSUMING == client->base.state);

        client->http_parser = malloc(sizeof(http_parser));
        client->http_parser->data = client;
        http_parser_init(client->http_parser, HTTP_REQUEST);

        client->base.state = HTTP_CLIENT_STATE_READING_REQUEST;
    }

    assert(HTTP_CLIENT_STATE_READING_REQUEST == client->base.state);

    http_parser_settings settings = empty_settings;
    settings.on_message_complete = on_request_message_complete;

    int nparsed = http_parser_execute(client->http_parser, &settings, data, size);
    if (nparsed != size) {
        client->base.state = HTTP_CLIENT_STATE_BAD_REQUEST;
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_WARNING
        printf("http_parser parse request error [fd %d]: nparsed != nreceived (%d vs %d)\n", sfd, nparsed, size);
#endif
        return -2;
    }

    worker *w = (worker *)clients_storage_get_client_by_sfd(client->worker_sfd);
    assert(NULL != w);
    worker_add_data_chunk(w, data, size);

    return 0;
}
