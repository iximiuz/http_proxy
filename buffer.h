#ifndef BUFFER_H
#define BUFFER_H

typedef struct _buffer {
    char *data;
    int size;            // count of data bytes in buf.data
    int capacity;        // count of allocated bytes in buf.data (should be always >= buf.size)
    int written_count;   // on write mode - count of bytes already written to socket.
} buffer;


int buffer_append(buffer *buf, const char *data, const unsigned int size);
void buffer_clear(buffer *buf);

#endif // BUFFER_H
