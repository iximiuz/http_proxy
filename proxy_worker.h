#ifndef PROXY_WORKER_H
#define PROXY_WORKER_H

#include "proxy_client.h"
#include "proxy_proto.h"


worker_packet *make_worker_net_packet(WP_TYPES type, wp_flags flags, const char *payload,
                                      const wp_size payload_size, wp_size *packet_size);


buffer *worker_add_data_chunk(worker *w, const char *data, const int size);
buffer *worker_get_top_buffer(worker *w);
void worker_clear_buffer(worker *a_worker);

int read_worker_packet(int sfd, const char *data, int size);
int write_worker_packet(worker *w);

int worker_process_incoming_packet(worker *w);

#endif // PROXY_WORKER_H
