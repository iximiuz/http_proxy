#include "clients_storage.h"
#include "proxy_daemon.h"
#include "proxy_worker.h"
#include "sockets.h"

#include <assert.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>


worker_packet *make_worker_net_packet(WP_TYPES type, wp_flags flags, const char *payload,
                                      const wp_size payload_size, wp_size *packet_size)
{
    assert(NULL != packet_size);

    worker_packet *packet = malloc(WP_HEADER_SIZE + payload_size);
    if (NULL != packet) {
        packet->type = (wp_type)type;
        packet->payload_size = payload_size;
        packet->flags = flags;
        memcpy(packet->payload, payload, payload_size);

        *packet_size = WP_HEADER_SIZE + payload_size;
    }

    return packet;
}

int forward_http_response(worker *a_worker, const char *data, const int size)
{
    // data [msg_type][msg_size][flags][msg_payload]

    assert(CLIENT_STATE_CONSUMING == a_worker->base.state && WORKER_STATE_READING_HTTP_RESPONSE == a_worker->base.next_state
           || WORKER_STATE_READING_HTTP_RESPONSE == a_worker->base.state);

    if (WP_TYPE_HTTP_RESPONSE != data[0]) {
        // bad response from worker
        return -1;
    }

    //wp_size payload_size = *(wp_size *(data + WP_TYPE_FIELD_SIZE));

//    if (0 < a_worker->base.buf.size) {
//        worker_add_data_chunk(a_worker, data, size);
//        return 0;
//    }

    return 0;

    // 0. Если соотв. клиент 1 типа был удален (отсоединился) - просто вычитываем все данные до конца сообщения
    // 1. Разобраить протокол.
    // 2. Выцепить непосредственно HTTP RESPONSE
    // 3. Записать HTTP RESPONSE (без промежуточной буферизации) в client->opponent_sfd
    // 4. На каждую порцию натравливать http_parser и ждать события on_message_complete.
    // 5. По событию message_complete закрыть соединение с клиентом 1 и сбросить состояние клиента 2.
}

buffer *worker_add_data_chunk(worker *w, const char *data, const int size)
{
    buffer *bucket = malloc(sizeof(buffer));
    if (NULL == bucket) {
        return NULL;
    }

    memset(bucket, 0, sizeof(buffer));
    buffer_append(bucket, data, size);

    if (NULL == w->buf) {
        w->buf = malloc(sizeof(chunked_buffer));
        if (NULL == w->buf) {
            buffer_clear(bucket);
            free(bucket);

            return NULL;
        }

        memset(w->buf, 0, sizeof(chunked_buffer));
        w->buf->bucket = bucket;
    } else {
        chunked_buffer *chunk = w->buf;
        while (NULL != chunk->next) {
            chunk = chunk->next;
        }

        chunk->next = malloc(sizeof(chunked_buffer));
        if (NULL == chunk->next) {
            buffer_clear(bucket);
            free(bucket);

            return NULL;
        }

        memset(chunk->next, 0, sizeof(chunked_buffer));
        chunk->next->bucket = bucket;
    }

    return bucket;
}

void worker_clear_buffer(worker *w)
{
    chunked_buffer *chunk = w->buf, *tmp = NULL;
    while (NULL != chunk) {
        buffer_clear(chunk->bucket);
        free(chunk->bucket);

        tmp = chunk;
        chunk = chunk->next;

        free(tmp);
    }

    w->buf = NULL;
}

buffer *worker_get_top_buffer(worker *w)
{
    chunked_buffer *chunk = w->buf;
    while (NULL != chunk) {
        if (NULL == chunk->next) {
            break;
        }

        chunk = chunk->next;
    }

    return NULL == chunk ? NULL : chunk->bucket;
}

static worker *safe_fetch_worker(int sfd)
{
    proxy_client *client = clients_storage_get_client_by_sfd(sfd);
    if (NULL == client) {
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_WARNING
        printf("Cannot find worker for descriptor [fd %d]. Stop reading.\n", sfd);
        fflush(stdout);
#endif
    } else {
            assert(client->type == CLIENT_TYPE_WORKER);
    }

    return (worker *)client;
}

int read_worker_packet(int sfd, const char *data, int size)
{
    worker *w = safe_fetch_worker(sfd);
    if (NULL == w) {
        return -1;
    }

    assert(CLIENT_STATE_CONSUMING == w->base.state || WORKER_STATE_PACKET_PROCESSING == w->base.state);

    if (WORKER_STATE_PACKET_PROCESSING == w->base.state) {
        w->base.state = WORKER_STATE_BAD_PROTO;
        return -1;
    }

    if (0 == size) {
        w->base.state = CLIENT_STATE_DISCONNECT;
        return -1;
    }

    buffer *buf = worker_get_top_buffer(w);
    if (NULL == buf) {
        buf = worker_add_data_chunk(w, data, size);
    } else {
        if (0 != buffer_append(buf, data, size)) {
            w->base.state = CLIENT_STATE_DISCONNECT;
            return -1;
        }
    }

    if (NULL == buf) {
        w->base.state = CLIENT_STATE_DISCONNECT;
        return -1;
    }

    if (WP_TYPE_NOT_ALLOWED_MIN >= buf->data[0] || buf->data[0] >= WP_TYPE_NOT_ALLOWED_MAX) {
        w->base.state = WORKER_STATE_BAD_PROTO;
        return -1;
    }

    wp_size payload_size;
    if (buf->size >= WP_TYPE_FIELD_SIZE + WP_SIZE_FIELD_SIZE) {
        payload_size = *((wp_size *)(buf->data + WP_TYPE_FIELD_SIZE));
        if (payload_size > WP_MAX_PACKET_PAYLOAD_SIZE) {
            w->base.state = WORKER_STATE_BAD_PROTO;
            return -1;
        }
    }

    if (buf->size > WP_HEADER_SIZE) {
        if (buf->size - WP_HEADER_SIZE > payload_size) {
            w->base.state = WORKER_STATE_BAD_PROTO;
            return -1;
        }

        if (buf->size - WP_HEADER_SIZE == payload_size) {
            w->base.state = WORKER_STATE_PACKET_PROCESSING;
            return 0;
        }
    }

    assert(CLIENT_STATE_CONSUMING == w->base.state);

    return 0;
}

int write_worker_packet(worker *w)
{
    assert(NULL != w);
    assert(CLIENT_STATE_SUPPLYING == w->base.state);

    buffer *buf = worker_get_top_buffer(w);
    assert(NULL != buf);
    assert(0 < buf->size);
    assert(buf->size - buf->written_count >= 0);
    assert(NULL != buf->data);

    if (buf->size > buf->written_count) {
        int written;
        if (
                SOCKET_IO_RES_SUCCESS != socket_write(w->base.sfd, buf->data + buf->written_count,
                                                      buf->size - buf->written_count, &written)
        ) {
            w->base.state = CLIENT_STATE_DISCONNECT;
            return -1;
        }

        buf->written_count += written;
        assert(buf->written_count <= buf->size);
    }

    if (buf->written_count == buf->size) {
        w->base.state = w->base.next_state;
        w->base.next_state = CLIENT_STATE_UNDEFINED;
    }

    return 0;
}

static int process_worker_handshake_packet(worker *w, worker_packet *packet)
{
    if (WORKER_STATE_INITIAL != w->base.prev_state) {
        w->base.state = WORKER_STATE_BAD_PROTO;
        return -1;
    }

    if (PROXY_PROTO_MSG_CONNECT_SIZE != packet->payload_size) {
        w->base.state = WORKER_STATE_BAD_PROTO;
        return -1;
    }

    if (0 != strncmp(PROXY_PROTO_MSG_CONNECT, packet->payload, PROXY_PROTO_MSG_CONNECT_SIZE)) {
        w->base.state = WORKER_STATE_BAD_PROTO;
        return -1;
    }

    worker_clear_buffer(w); // удаляет и packet, так как packet и worker buf одно и то же в этот момент

    wp_size packet_size;
    worker_packet *handshake_response = make_worker_net_packet(WP_TYPE_HANDSHAKE_RESPONSE, 0,
                                                               PROXY_PROTO_MSG_CONNECT_RESPONSE,
                                                               PROXY_PROTO_MSG_CONNECT_RESPONSE_SIZE,
                                                               &packet_size);
    if (NULL == worker_add_data_chunk(w, (const char *)handshake_response, packet_size)) {
        w->base.state = CLIENT_STATE_DISCONNECT;
        return -1;
    }

    w->base.state = WORKER_STATE_HANDSHAKE;
    return 0;
}

static int process_worker_handshake_packet(worker *w, worker_packet *packet)
{
    w->base.state = WORKER_STATE_FORWARD_HTTP_RESPONSE;
}

int worker_process_incoming_packet(worker *w)
{
    assert(NULL != w);
    assert(WORKER_STATE_PACKET_PROCESSING == w->base.state);

    buffer *buf = worker_get_top_buffer(w);
    assert(NULL != buf);
    assert(0 < buf->size);
    assert(NULL != buf->data);

    worker_packet *packet = (worker_packet *)buf->data;

    switch (packet->type) {
    case WP_TYPE_HANDSHAKE_REQUEST:
        return process_worker_handshake_packet(w, packet);
    break;

    case WP_TYPE_HTTP_RESPONSE:
        return process_worker_http_response_packet(w, packet);
        break;

    default:
        abort();
    }
}
