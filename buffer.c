#include "buffer.h"

#include <assert.h>
#include <memory.h>
#include <stdlib.h>


int buffer_append(buffer *buf, const char *data, const unsigned int size)
{
    assert(NULL != buf);

    if (buf->capacity - buf->size < size) {
        // if buf->data == NULL realloc looks like malloc
        char *buffer = realloc(buf->data, buf->capacity + 2 * size);
        if (NULL == buffer) {
            return -1;
        }

        buf->data = buffer;
        buf->capacity = buf->capacity + 2 * size;
    }

    memcpy(buf->data + buf->size, data, size);
    buf->size = buf->size + size;

    return 0;
}

void buffer_clear(buffer *buf)
{
    assert(NULL != buf);

    if (NULL != buf->data) {
        free(buf->data);
        buf->data = NULL;
    }

    buf->capacity = 0;
    buf->size = 0;
    buf->written_count = 0;
}
