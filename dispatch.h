#ifndef DISPATCH_H
#define DISPATCH_H

#define CAN_NOT_CREATE_OR_BIND_SOCKET -1
#define CAN_NOT_START_LISTEN_SOCKET   -2
#define CAN_NOT_CREATE_EPOLL          -3
#define CAN_NOT_ADD_FD_TO_EPOLL       -4

int dispatch(const char *host, const char *http_clien_port, const char *worker_port);

#endif // DISPATCH_H
