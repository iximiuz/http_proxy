#ifndef HTTP_CLIENT_EVENTS_H
#define HTTP_CLIENT_EVENTS_H

#include "proxy_client.h"

typedef enum {
    HTTP_CLIENT_EVENT_NO_EVENT,
    HTTP_CLIENT_EVENT_TRANSIT,  // unconditional transition
    HTTP_CLIENT_EVENT_DATA_IN,  // Направление данных "от воркера к демону"
    HTTP_CLIENT_EVENT_DATA_OUT, // Направление данных "от демона к воркеру"
    HTTP_CLIENT_EVENT_ABORT
} HTTP_CLIENT_EVENTS;


HTTP_CLIENT_EVENTS do_http_client_state_transition(http_client *hc, HTTP_CLIENT_EVENTS event);


#endif // HTTP_CLIENT_EVENTS_H
