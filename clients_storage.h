#ifndef CLIENTS_STORAGE_H
#define CLIENTS_STORAGE_H

#include "proxy_client.h"

proxy_client *clients_storage_add_client(const int sfd, const CLIENT_TYPES type);
proxy_client *clients_storage_get_client_by_sfd(int sfd);
int clients_storage_remove_client(int sfd);

http_client *clients_storage_select_pending_http_client();
worker *clients_storage_select_pending_worker();

#endif // CLIENTS_STORAGE_H
