#include "proxy_client.h"
#include "proxy_daemon.h"

#include <memory.h>
#include <stdlib.h>


typedef struct _client_node {
    proxy_client *client;
    struct _client_node *prev;
    struct _client_node *next;
} client_node;

static client_node *first_client = NULL;


static client_node *alloc_client_node()
{
    client_node *client = malloc(sizeof(client_node));
    if (NULL == client) {
        return NULL;
    }

    client->next = NULL;
    client->prev = NULL;

    return client;
}


proxy_client *clients_storage_add_client(const int sfd, const CLIENT_TYPES type)
{
    proxy_client *client = NULL;
    size_t client_size = CLIENT_TYPE_HTTP_CLIENT == type
            ? sizeof(http_client)
            : sizeof(worker);

    client = malloc(client_size);
    if (NULL == client) {
        return NULL;
    }

    memset(client, 0, client_size);
    client->sfd = sfd;
    client->type = type;
    client->state = CLIENT_TYPE_HTTP_CLIENT == type
            ? HTTP_CLIENT_STATE_CONNECTED
            : WORKER_STATE_INITIAL;
    client->next_state = CLIENT_STATE_UNDEFINED;
    client->prev_state = CLIENT_STATE_UNDEFINED;
    if (CLIENT_TYPE_HTTP_CLIENT == type) {
        ((http_client *)client)->worker_sfd = UNDEFINED_SFD;
    } else {
        ((worker *)client)->http_client_sfd = UNDEFINED_SFD;
    }

    if (NULL == first_client) {
        first_client = alloc_client_node();
        if (NULL == first_client) {
            free(client);
            return NULL;
        }

        first_client->client = client;
    } else {
        client_node *cur = first_client;
        while (NULL != cur->next) {
            cur = cur->next;
        }

        cur->next = alloc_client_node();
        if (NULL == cur->next) {
            free(client);
            return NULL;
        }

        cur->next->client = client;
        cur->next->prev = cur;
    }

    return client;
}

proxy_client *clients_storage_get_client_by_sfd(int sfd)
{
    client_node *client = first_client;
    while (NULL != client) {
        if (client->client->sfd == sfd) {
            return client->client;
        }

        client = client->next;
    }

    return NULL;
}

int clients_storage_remove_client(int sfd)
{
    client_node *client = first_client;

    while (NULL != client) {
        if (client->client->sfd == sfd) {
            if (NULL != client->prev) {
                client->prev->next = client->next;
            }

            if (NULL != client->next) {
                client->next->prev = client->prev;
            }

            if (CLIENT_TYPE_WORKER == client->client->type) {
                worker_clear_buffer((worker *)client->client);
            } else {
                free(((http_client *)client->client)->http_parser);
            }

            free(client->client);
            free(client);

            return 1;
        }

        client = client->next;
    }

    return 0;
}

static proxy_client *find_client(const CLIENT_TYPES type, const CLIENT_STATES state)
{
    client_node *client = first_client;

    while (NULL != client) {
        if (type == client->client->type && client->client->state) {
            return client->client;
        }

        client = client->next;
    }

    return NULL;
}

http_client *clients_storage_select_pending_http_client()
{
    return (http_client *)find_client(CLIENT_TYPE_HTTP_CLIENT, HTTP_CLIENT_STATE_CONNECTED);
}

worker *clients_storage_select_pending_worker()
{
    return (worker *)find_client(CLIENT_TYPE_WORKER, WORKER_STATE_READY);
}
