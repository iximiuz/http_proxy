#ifndef PROXY_DAEMON_H
#define PROXY_DAEMON_H

#include "proxy_client.h"

#define UNDEFINED_PORT -1
#define UNDEFINED_SFD  -1


struct proxy_daemon {
    int ports[CLIENT_TYPES_COUNT];
    int fds[CLIENT_TYPES_COUNT];
    int incoming_connections_count[CLIENT_TYPES_COUNT];
};

void reset_daemon(struct proxy_daemon *daemon);

#endif // PROXY_DAEMON_H
