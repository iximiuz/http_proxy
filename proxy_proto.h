#ifndef PROXY_PROTO_H
#define PROXY_PROTO_H

#include <stdint.h>

#define PROXY_PROTO_MSG_CONNECT                "ping"
#define PROXY_PROTO_MSG_CONNECT_SIZE           (sizeof(PROXY_PROTO_MSG_CONNECT) - 1)
#define PROXY_PROTO_MSG_CONNECT_RESPONSE       "pong"
#define PROXY_PROTO_MSG_CONNECT_RESPONSE_SIZE  (sizeof(PROXY_PROTO_MSG_CONNECT_RESPONSE) - 1)

typedef /* signed */ char wp_type;
typedef int32_t wp_size;
typedef unsigned char wp_flags;

#define WP_TYPE_FIELD_SIZE  sizeof(wp_type)
#define WP_SIZE_FIELD_SIZE  sizeof(wp_size)
#define WP_FLAGS_FIELD_SIZE sizeof(wp_flags)
#define WP_HEADER_SIZE      (WP_TYPE_FIELD_SIZE + WP_SIZE_FIELD_SIZE + WP_FLAGS_FIELD_SIZE)

#define WP_FLAG_RST 0x02
#define WP_FLAG_FIN 0x03

#define WP_MAX_PACKET_PAYLOAD_SIZE 1024

// Worker Packet types
typedef enum {
    WP_TYPE_NOT_ALLOWED_MIN,
    WP_TYPE_HANDSHAKE_REQUEST,
    WP_TYPE_HANDSHAKE_RESPONSE,
    WP_TYPE_HTTP_REQUEST,
    WP_TYPE_HTTP_RESPONSE,
    WP_TYPE_HTTP_RESPONSE_ACK
    WP_TYPE_NOT_ALLOWED_MAX
} WP_TYPES;


#pragma pack(push, 1)
typedef struct _worker_packet {
    wp_type type;
    wp_size payload_size;
    wp_flags flags;
    char payload[];
} worker_packet;
#pragma pack(pop)

#endif // PROXY_PROTO_H
