#ifndef SOCKETS_H
#define SOCKETS_H

#define SOCKETS_READ_BUFF_SIZE 1024

typedef int (*on_socket_data_cb)(int sfd, const char *data, int size);
typedef enum {
    SOCKET_IO_RES_SUCCESS = 0,
    SOCKET_IO_RES_SYS_ERROR = -1,
    SOCKET_IO_RES_DISCONNECT = -2,
    SOCKET_IO_RES_CB_ERROR = -3
} SOCKET_IO_RES;


int create_and_bind_non_blocking_socket(const char *port, const char *host);
int make_socket_non_blocking(int sfd);
SOCKET_IO_RES socket_read(int sfd, on_socket_data_cb on_read_cb);
SOCKET_IO_RES socket_write(int sfd, const char *data, int size, int *written);

#endif // SOCKETS_H
