#define HTTP_PROXY_LOG_LEVEL_DEBUG   5
#define HTTP_PROXY_LOG_LEVEL_NOTICE  4
#define HTTP_PROXY_LOG_LEVEL_WARNING 3
#define HTTP_PROXY_LOG_LEVEL_ERROR   2
#define HTTP_PROXY_LOG_LEVEL_ALERT   1

#include "dispatch.h"
#include "proxy_client.h"
#include "proxy_daemon.h"

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>


extern struct proxy_daemon g_daemon;

void show_status(int sig_num)
{
    signal(SIGUSR1, show_status);

    printf("Daemon is working on [HTTP client's port %d] [worker's port %d]\n",
           g_daemon.ports[CLIENT_TYPE_HTTP_CLIENT], g_daemon.ports[CLIENT_TYPE_WORKER]);
    printf("*** Stat ***\n\tactive incoming connections from HTTP CLIENTS [%d]\n",
           g_daemon.incoming_connections_count[CLIENT_TYPE_HTTP_CLIENT]);
    printf("\tactive incoming connections from WORKERS [%d]\n",
           g_daemon.incoming_connections_count[CLIENT_TYPE_WORKER]);

    fflush(stdout);
}


int main(int argc, char *argv[])
{
    int s;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s <http-client's-port> <worker's-port>\n", argv[0]);
        return 1;
    }

    // freopen("log.txt", "w", stdout);
    // freopen("log.txt", "w", stderr);

    reset_daemon(&g_daemon);

    g_daemon.ports[CLIENT_TYPE_HTTP_CLIENT] = atoi(argv[1]);
    g_daemon.ports[CLIENT_TYPE_WORKER]      = atoi(argv[2]);

    signal(SIGUSR1, show_status);

    printf("Daemon (pid %d) started on [HTTP client's port %d] [worker's port %d]\n",
           getpid(), g_daemon.ports[CLIENT_TYPE_HTTP_CLIENT], g_daemon.ports[CLIENT_TYPE_WORKER]);
    printf("Use 'kill -USR1 %d' to show daemon status\n\n", getpid());
    fflush(stdout);

    s = dispatch(NULL, argv[1], argv[2]);
    if (0 != s) {
        fprintf(stderr, "Error: dispatch returns [%d]\n", s);
        return 2;
    }

    return 0;
}
