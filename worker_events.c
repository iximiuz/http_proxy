#include "clients_storage.h"
#include "http_client_events.h"
#include "proxy_proto.h"
#include "proxy_worker.h"
#include "sockets.h"
#include "worker_events.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

static WORKER_EVENTS handle_transition_from_initial_state(worker *w, WORKER_EVENTS event,
                                                          http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_INITIAL == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_DATA_IN:
        w->base.prev_state = w->base.state;
        w->base.state = CLIENT_STATE_CONSUMING;
        return WORKER_EVENT_TRANSIT;
        break;

    // просто пропускаем такое событие
    case WORKER_EVENT_DATA_OUT:
        return WORKER_EVENT_NO_EVENT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
    }
}

static WORKER_EVENTS handle_transition_from_consuming_state(worker *w, WORKER_EVENTS event,
                                                            http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(CLIENT_STATE_CONSUMING == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_DATA_IN:
    case WORKER_EVENT_TRANSIT:
        if (SOCKET_IO_RES_SYS_ERROR == socket_read(w->base.sfd, read_worker_packet)) {
            w->base.state = CLIENT_STATE_DISCONNECT;
        }

        assert(CLIENT_STATE_DISCONNECT == w->base.state
               || CLIENT_STATE_CONSUMING == w->base.state
               || WORKER_STATE_BAD_PROTO == w->base.state
               || WORKER_STATE_PACKET_PROCESSING == w->base.state);

        return CLIENT_STATE_CONSUMING == w->base.state
                ? WORKER_EVENT_NO_EVENT
                : WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_supplying_state(worker *w, WORKER_EVENTS event,
                                                            http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(CLIENT_STATE_SUPPLYING == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_TRANSIT:
        write_worker_packet(w);

        assert(CLIENT_STATE_DISCONNECT == w->base.state
               || CLIENT_STATE_SUPPLYING == w->base.state
               || WORKER_STATE_READY == w->base.state
               || WORKER_STATE_WRITING_HTTP_REQUEST == w->base.state
               || WORKER_STATE_HTTP_REQUEST_PROCESSING == w->base.state);

        return CLIENT_STATE_SUPPLYING == w->base.state
                ? WORKER_EVENT_NO_EVENT
                : WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_packet_processing_state(worker *w, WORKER_EVENTS event,
                                                                    http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_PACKET_PROCESSING == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_TRANSIT:
        worker_process_incoming_packet(w);
        return WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_handshake_state(worker *w, WORKER_EVENTS event,
                                                            http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_HANDSHAKE == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_TRANSIT:
        w->base.prev_state = w->base.state;
        w->base.state = CLIENT_STATE_SUPPLYING;
        w->base.next_state = WORKER_STATE_READY;

        return WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_ready_state(worker *w, WORKER_EVENTS event,
                                                        http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_READY == w->base.state);
    assert(NULL == *hc);
    assert(WORKER_STATE_HANDSHAKE == w->base.prev_state);

    http_client *hcc;

    switch (event) {
    case WORKER_EVENT_TRANSIT:
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_DEBUG
        printf("[+] Connected new worker [fd %d]\n", w->base.sfd);
        fflush(stdout);
#endif
        // todo: if exists pending HTTP client - serve it

        return WORKER_EVENT_NO_EVENT;
        break;

    // вероятно пришел EOF по сети, так как произошел
    // дисконнект воркера
    case WORKER_EVENT_DATA_IN:
        w->base.prev_state = w->base.state;
        w->base.state = CLIENT_STATE_CONSUMING;
        return WORKER_EVENT_TRANSIT;
        break;

    case WORKER_EVENT_NEW_HTTP_CLIENT:
        hcc = (http_client *)clients_storage_get_client_by_sfd(w->http_client_sfd);
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_writing_http_request_state(worker *w, WORKER_EVENTS event,
                                                                       http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_WRITING_HTTP_REQUEST == w->base.state);
    assert(NULL == *hc);

    buffer *buf;

    switch (event) {
    case WORKER_EVENT_NEW_HTTP_CLIENT:
        // в буфере воркера лежит кусок (или весь) HTTP-запрос, его нужно обернуть в пакет и отправить
        buf = worker_get_top_buffer(w);
        assert(NULL != buf);

        wp_size packet_size = 0;
        worker_packet *packet = make_worker_net_packet(WP_TYPE_HTTP_REQUEST,
                                                       WORKER_STATE_HTTP_REQUEST_PROCESSING == w->base.next_state
                                                         ? WP_FLAG_FIN
                                                         : 0,
                                                       buf->data, buf->size, &packet_size);
        if (NULL == packet) {
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_ERROR
            fprintf(stderr, "Cannot alloc memory for worker packet\n");
#endif
            w->base.state = CLIENT_STATE_DISCONNECT;
            return WORKER_EVENT_TRANSIT;
        }

        worker_clear_buffer(w);
        worker_add_data_chunk(w, (const char *)packet, packet_size);

        w->base.prev_state = w->base.state;
        w->base.state = CLIENT_STATE_SUPPLYING;

        return WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_http_request_processing_state(worker *w, WORKER_EVENTS event,
                                                                          http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_HTTP_REQUEST_PROCESSING == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_DATA_IN:
    case WORKER_EVENT_TRANSIT:
        worker_clear_buffer(w);

        w->base.prev_state = w->base.state;
        w->base.next_state = WORKER_STATE_READING_HTTP_RESPONSE;
        w->base.state = CLIENT_STATE_CONSUMING;

        return WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

static WORKER_EVENTS handle_transition_from_forward_http_response_state(worker *w, WORKER_EVENTS event,
                                                                        http_client **hc, HTTP_CLIENT_EVENTS *hc_event)
{
    assert(NULL != w);
    assert(WORKER_STATE_HTTP_REQUEST_PROCESSING == w->base.state);
    assert(NULL == *hc);

    switch (event) {
    case WORKER_EVENT_TRANSIT:
        // нужно отправить packet payload клиенту, а воркеру записать ack

        return WORKER_EVENT_TRANSIT;
        break;

    default:
        // log and return disconnect event
        abort();
        return WORKER_EVENT_NO_EVENT;
        break;
    }
}

WORKER_EVENTS do_worker_state_transition(worker *w, WORKER_EVENTS event)
{
    http_client *hc = NULL;
    HTTP_CLIENT_EVENTS hc_event;
    WORKER_EVENTS next_event = WORKER_EVENT_NO_EVENT;

    switch (w->base.state) {
    case WORKER_STATE_INITIAL:
        next_event = handle_transition_from_initial_state(w, event, &hc, &hc_event);
        break;

    case CLIENT_STATE_CONSUMING:
        next_event = handle_transition_from_consuming_state(w, event, &hc, &hc_event);
        break;

    case CLIENT_STATE_SUPPLYING:
        next_event = handle_transition_from_supplying_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_PACKET_PROCESSING:
        next_event = handle_transition_from_packet_processing_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_HANDSHAKE:
        next_event = handle_transition_from_handshake_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_READY:
        next_event = handle_transition_from_ready_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_WRITING_HTTP_REQUEST:
        next_event = handle_transition_from_writing_http_request_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_HTTP_REQUEST_PROCESSING:
        next_event = handle_transition_from_http_request_processing_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_READING_HTTP_RESPONSE:

        break;

    case WORKER_STATE_FORWARD_HTTP_RESPONSE:
        next_event = handle_transition_from_forward_http_response_state(w, event, &hc, &hc_event);
        break;

    case WORKER_STATE_BAD_PROTO:
        printf("WORKER_STATE_BAD_PROTO [fd %d]\n", w->base.sfd);
        fflush(stdout);

        w->base.state = CLIENT_STATE_DISCONNECT;
        next_event = WORKER_EVENT_TRANSIT;
        break;

    case CLIENT_STATE_DISCONNECT:
        // todo: find related HTTP client and disconnect it

        printf("CLIENT_STATE_DISCONNECT [fd %d]\n", w->base.sfd);
        fflush(stdout);

        return WORKER_EVENT_ABORT;
        break;

    default:
#if HTTP_PROXY_LOG_LEVEL >= HTTP_PROXY_LOG_LEVEL_ALERT
        fprintf(stderr, "Unexpected worker [fd %d] state [%d]\n", w->base.sfd, w->base.state);
#endif
        abort();
    }

    return WORKER_EVENT_NO_EVENT == next_event
            ? WORKER_EVENT_NO_EVENT
            : do_worker_state_transition(w, next_event);
}
